//large medium small
let BDSIXcaps = [0.78, 51.64, 46.22].map(function(element) {
    return element*13.92;
});
let PSGIXcaps = [1.29, 58.54, 39.09].map(function(element) {
    return element*21.10;
});
let MALRXcaps = [85.26, 13.65, 0].map(function(element) {
    return element*17.14;
});
let CMVIXcaps = [87.54, 11.48, 0].map(function(element) {
    return element*16.58;
});
let MALVXcaps = [82.66, 16.13, 0].map(function(element) {
    return element*29.63;
});
let BROIXcaps = [78.15, 17.09, 1.15].map(function(element) {
    return element*15.84;
});
let BLSIXcaps = [73.54, 24.73, 4.46].map(function(element) {
    return element*9.54;
});

//size of comps depends on the number of funds chosen
var comps = [];
// comps.push(BDSIXcaps);
// comps.push(PSGIXcaps);
// comps.push(MALRXcaps);

var thetas;
resetThetas();

//-----------------------------------

const learningRate = 0.00000025;
const optimizer = tf.train.sgd(learningRate);

function predict(comps) {
    var guessys = tf.tensor([0,0,0]);
    // guessys = guessys.add(tf.tensor(comps[0]).mul(thetas[0]));
    for (var i = 0; i < comps.length; i++) {
        guessys = guessys.add(tf.tensor(comps[i]).mul(thetas[i]));
    }
    return guessys;
}

//pred are the guessys from predict()
//labels are the 33 percent
function loss(pred, labels) {
    return pred.sub(labels).square().mean();
}

// computeMinThetas();

function resetThetas() {
    thetas = [];
    for (var i = 0; i < comps.length; i++) {
        thetas.push(tf.variable(tf.scalar(Math.random())));
    }
}

function computeMinThetas(maxFunds, comps) {
    var count = 0;
    var maxRecalc = 10;

    var values = thetas.map(t => t.dataSync()[0]);
    while (count < 10) {
        count = count + 1;
        // console.log(count);
        // if (count > 10) {
        //     break;
        // }
        // console.log("looping");
        // comps = [];
        this.comps = [];
        this.comps = comps;
        var ys = [maxFunds/3.0, maxFunds/3.0, maxFunds/3.0];
        resetThetas(comps);

        // let numIterations = 75;
        let numIterations = 75;
        for (let iter = 0; iter < numIterations; iter++) {
            optimizer.minimize(function() {
                return loss(predict(comps), ys);
            });
        }

        values = thetas.map(t => t.dataSync()[0]);
        var allPos = true;
        for (var i = 0; i < values.length; i++) {
            if (values[i] < 0) {
                allPos = false;
            }
        }
        if (allPos) {
            break;
        }
    }

    return thetas;
}
