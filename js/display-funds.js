var bdsix = {
    displayName: "Advantage Small Cap Core Fund (BDSIX)",
    caps: BDSIXcaps,
    value: "BDSIX", // value needed to access API
    price: 13.92,
};

var psgix = {
    displayName: "Advantage Small Cap Growth Fund (PSGIX)",
    caps: PSGIXcaps,
    value: "PSGIX", // value needed to access API
    price: 21.10,
};

var malrx = {
    displayName: "Advantage Large Cap Core Fund (MALRX)",
    caps: MALRXcaps,
    value: "MALRX", // value needed to access API
    price: 17.14,
};

var cmvix = {
    displayName: "Advantage Large Cap Growth Fund(CMVIX)",
    caps: CMVIXcaps,
    value: "CMVIX", // value needed to access API
    price: 16.58,
};

var malvx = {
    displayName: "Advantage Large Cap Value Fund(MALVX)",
    caps: MALVXcaps,
    value: "MALVX", // value needed to access API
    price: 29.63,
};

var broix = {
    displayName: "Advantage International Fund (BROIX)",
    caps: BROIXcaps,
    value: "BROIX", // value needed to access API
    price: 15.84,
};

var blsix = {
    displayName: "Advantage Emerging Markets Fund (BLSIX)",
    caps: BLSIXcaps,
    value: "BLSIX", // value needed to access API
    price: 9.54,
};

var funds = {
    "BDSIX": bdsix,
    "PSGIX": psgix,
    "MALRX": malrx,
    "CMVIX": cmvix,
    "MALVX": malvx,
    "BROIX": broix,
    "BLSIX": blsix,
}

// var funds = [
//     bdsix,
//     psgix,
//     malrx,
//     cmvix,
//     malvx,
//     broix,
//     blsix,
// ]

var fundList = $("#fund-list");
fundList.html("");
for (var i in funds){
    if (typeof funds[i] !== 'function') {
        // console.log("Key is " + k + ", value is" + funds[k]);
        var inputhtml = `
        <div class = 'fund-list-item'>
        <input type='checkbox' name='fund' value='${funds[i].value}'> ${funds[i].displayName} <br>
        </div>
        `;
        fundList.append(inputhtml);
    }
}

$(document).ready(function() {
    $("#calculate-button").click(function(){
        var selectedFunds = [];
        $.each($("input[name='fund']:checked"), function(){
            selectedFunds.push($(this).val());
            // console.log(funds[$(this).val()]);
        });
        var comps = [];
        for (var i in selectedFunds){
            comps.push(funds[selectedFunds[i]].caps);
        }


        var thetas = computeMinThetas($("#capital-input").val(), comps);
        let values = thetas.map(t => t.dataSync()[0])
        console.log(values);
        $("#results").html("");
        var resulthtml = "";
        var total = 0;
        for(var i = 0; i < values.length; i++) {
            var val = values[i];

            resulthtml += `
            <div class = 'result'>
                ${parseFloat(val * 100).toFixed(2)} shares of ${selectedFunds[i]}
            </div>
            `;
            total += val * 100 * funds[selectedFunds[i]].price;
        }
        $("#results").append("<strong>You should aim to buy:</strong>");
        $("#results").append(resulthtml);
        $("#results").append("<strong>Total spent: $" + parseFloat(total).toFixed(2).toLocaleString() + "</strong><br>");
        $("#results").append("<div id = 'recalculate-advice'><strong>Not liking what you're seeing? Try hitting calculate again! </strong></div>");
    });
});



