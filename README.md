# HackGT Mutual Fund Market Cap Equalizer

## Contributors:
### Justin Li
### Scott Lombard
### Ajay Vijayakumar
### Gabby Germanson

 *Functionality*

 This calculator is designed to take in an input of desired investments
 and available capital. The purpose of the equalizer is to proved an equally
 weighted, 100% equity portfolio by using the funds' market cap distributions
 (small-cap, mid-cap, and large-cap). Behind the scenes the machine learning plugin, TenserFlow, computes the target share quantity based upon the price, cap, and capital.
 Each run triggers 75 iterations of the algorithm, providing a tolerable margin of error.
 
*Assumptions*

1. Fractional shares can be purchased.
2. Selected funds will not have a singular market cap focus (e.g. 3 large cap mutual      funds).
3. The users of this product, for now, are institutional investors.

#### *Thank you HackGT!!*
